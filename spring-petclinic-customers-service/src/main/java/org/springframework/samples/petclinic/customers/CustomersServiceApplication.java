/*
 * Copyright 2002-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.customers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

// Cette annotation est utilisée pour indiquer que l'auteur de cette classe est Maciej Szarlinski.
/**
 * @author Maciej Szarlinski
 */

// Cette annotation est utilisée pour activer le client de découverte pour Spring Cloud.
// Cela permet à cette application de s'enregistrer avec un service de découverte, comme Eureka ou Consul.
@EnableDiscoveryClient

// Cette annotation est une commodité qui ajoute toutes les annotations suivantes :
// @Configuration : indique que la classe peut être utilisée par le conteneur Spring IoC comme source de définitions de beans.
// @EnableAutoConfiguration : indique à Spring Boot de commencer à ajouter des beans en fonction des paramètres de chemin de classe, d'autres beans et de divers paramètres de propriété.
// @ComponentScan : indique à Spring de rechercher d'autres composants, configurations et services dans le package de la classe, permettant ainsi de trouver les contrôleurs.
@SpringBootApplication

// Déclare une classe publique appelée CustomersServiceApplication.
public class CustomersServiceApplication {

	// Déclare la méthode principale qui est le point d'entrée de l'application Java.
	public static void main(String[] args) {
		// La méthode run démarre une nouvelle application Spring Boot en créant un nouveau contexte ApplicationContext 
		// et en retournant une instance de l'application.
		SpringApplication.run(CustomersServiceApplication.class, args);
	}
}