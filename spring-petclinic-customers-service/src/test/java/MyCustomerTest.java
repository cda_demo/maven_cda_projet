
// Importation des classes nécessaires
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.samples.petclinic.customers.CustomersServiceApplication;
// Cette annotation indique que le contexte de l'application Spring doit être chargé pour les tests.
// La classe CustomersServiceApplication est utilisée pour configurer le contexte.
@SpringBootTest(classes = CustomersServiceApplication.class)

// Cette annotation indique que le MockMvc doit être configuré automatiquement.
@AutoConfigureMockMvc

// Déclare une classe publique pour les tests.
public class MyCustomerTest {

  // Cette annotation est utilisée pour injecter le MockMvc dans cette classe de test.
  @Autowired
  private MockMvc mockMvc;

  // Cette annotation indique que la méthode suivante est un test.
  @Test
  public void monTest() throws Exception {
    // Utilise MockMvc pour effectuer une requête GET sur l'URL "/owners/1".
    mockMvc.perform(MockMvcRequestBuilders.get("/owners/1"))
        // Vérifie que le statut de la réponse est OK (200).
        .andExpect(MockMvcResultMatchers.status().isOk());
  }
}

//Ce code est un test d'intégration pour une application Spring Boot. Il utilise MockMvc pour envoyer une requête GET à l'URL "/owners/1" et vérifie que le statut de la réponse est OK (200). Si le statut de la réponse est différent de 200, le test échouera.