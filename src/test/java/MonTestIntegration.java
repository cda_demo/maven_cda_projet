// Importation des classes nécessaires
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

// Annotation indiquant qu'il s'agit d'un test Spring Boot. 
// L'argument webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT signifie que le serveur va démarrer sur un port aléatoire.
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MonTestIntegration {

  // Annotation pour injecter le port sur lequel le serveur a démarré.
  @LocalServerPort
  private int port;

  // Annotation pour injecter une instance de TestRestTemplate, qui est utilisée pour envoyer des requêtes HTTP.
  @Autowired
  private TestRestTemplate restTemplate;

  // Annotation indiquant qu'il s'agit d'une méthode de test.
  @Test
  public void monTest() {
    // Envoie une requête GET à l'URL de votre service et stocke la réponse.
    ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/mon-endpoint", String.class);
    
    // Vérifie que le statut de la réponse est 200 (OK).
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    
    // Vous pouvez également vérifier le corps de la réponse si nécessaire.
  }
}