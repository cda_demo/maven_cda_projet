// Importe l'annotation Test de JUnit 5 (JUnit Jupiter)
import org.junit.jupiter.api.Test;

// Importe la méthode assertEquals de JUnit 5 pour les assertions
import static org.junit.jupiter.api.Assertions.assertEquals;

// Déclare une classe publique pour les tests unitaires
public class MyServerUnitTest {

  // Annoté avec @Test pour indiquer que c'est une méthode de test
  @Test
  public void simpleTest() {
    // Définit une valeur attendue pour le test
    int expected = 1;

    // Définit une valeur réelle qui sera comparée à la valeur attendue
    int actual = 1;

    // Utilise assertEquals pour vérifier que la valeur réelle est égale à la valeur attendue
    // Si elles ne sont pas égales, le message "The actual value should be equal to the expected value" sera affiché
    assertEquals(expected, actual, "The actual value should be equal to the expected value");
  }

}